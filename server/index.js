
var config = {

	port : 11001

}

var http = require('http'),
    fs = require('fs'),
    _ = require('underscore'),
    // NEVER use a Sync function except at start-up!
    index = fs.readFileSync(__dirname + '/../client/background.jpg');


var masterPixelArray = [];


// Send client background image to all requests
var app = http.createServer(function(req, res) {
    res.writeHead(200, {'Content-Type': 'image/jpeg'});
    res.end(index);
});

// Socket.io server listens to our app
var io = require('socket.io').listen(app);

// connection
io.on('connection', function(socket){
  console.log('Connection', socket);

  socket.on('pixelArrayChange', function (data, from) {
	   console.log('I received a message by ', from, ' saying ', data);
	   detectChangesToMaster(data, function(err, changed) {
	   		if (changed) {
	   			console.log('masterPixelArrayChange', masterPixelArray);
	   			io.emit('masterPixelArrayChange', masterPixelArray);
	   		}
	   });
	});

});


var detectChangesToMaster = function(data, callback) {


console.log('detectChangesToMaster');
console.log('masterPixelArray', masterPixelArray);
console.log('old', data.old);
console.log('new', data.new);

	var originalMasterPixelArray = masterPixelArray;

	// first remove the old data
	masterPixelArray = _.difference(masterPixelArray, data.old);

console.log('out with the old', masterPixelArray);

	// now add in the new
	masterPixelArray = _.union(masterPixelArray, data.new);

console.log('in with the new', masterPixelArray);

	// has it changed ?
	if (!_.isEqual(originalMasterPixelArray, masterPixelArray)) {
		callback(null, true);
	} else {
		callback(null, false);
	}

}







app.listen(config.port);

