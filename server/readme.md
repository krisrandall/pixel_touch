# The Server Component of Pixel Touch

### Prerequisites
 `nodeJS` 
 
 
### Setup
 
    $ npm install

And to run the server    
    
    $ node index.js 

### Config

The socket server is configured to run on port 11001 - there is a configuration item at the top of `index.js` to configure that.
