


var touchesDetector = (function (){

	var config = {

		numPixelsWide : 10,
		numPixelsHigh : 20,

		touchStyle : {'background-color': 'white'},
		noTouchStyle : {'background': 'none'},

		onChange : function(newPixelArray, oldArray, diff) { console.log('Change', newPixelArray); }

	};



	var touchedPixels = [];
	var allPixelsArray = [];

	var visibleTouchCircles = [];



	var createPixelDivs = function() {

	    var pixelWidthPer = 100 / config.numPixelsWide;
	    var pixelHeightPer = 100 / config.numPixelsHigh;

		for (var py=0; py<=config.numPixelsHigh; py++) { 

			var newRow = $('<div>').addClass('tr')
									.attr('height', Math.round(pixelHeightPer)+"%");

			for (var px=0; px<=config.numPixelsWide; px++) {

			    // x and y are the middle of the touched pixel as a percentage (1 to 0) of the canvas side, to 2 decimal places
			    var x = (Math.round( (px / config.numPixelsWide) *100)/100);
			    var y = (Math.round( (py / config.numPixelsHigh) *100)/100);

			    // this relies on a 
				var pixel = { 
					x : x, 
					y: y
				};
				allPixelsArray.push(pixel);

				var newCell = $('<div>&nbsp;</div>').addClass('td')
										.addClass('pixel')
										.attr('id', allPixelsArray.length)
										.attr('width', Math.round(pixelWidthPer)+"%");


				newRow.append(newCell);
			}

			$(config.canvas).append(newRow);

		}

	}

	function whyDoesDamianAlwaysInsistOnFloats(sensibleNumber) {
		return parseFloat((sensibleNumber / 255).toFixed(3));
	}

	var detectPixelTouched = function(finger, touchEvent) {

		var aPixel = $('.pixel').first();

	    var tx = touchEvent.pageX + (aPixel.width()/2);
	    var ty = touchEvent.pageY + (aPixel.height()/2);

	    var p = config.canvas;
	    // ok to recalc these in here because device orientation would change it
		var canvasWidth = $(p).width();
		var canvasHeight = $(p).height();

	    // px and py are which pixel is being touched
	    var px = Math.round(tx / canvasWidth * config.numPixelsWide);
	    var py = Math.round(ty / canvasHeight * config.numPixelsHigh);

	    // x and y are the touched pixel as a percentage (1 to 0) of the canvas side, to 2 decimal places
	    var x = (Math.round( (px / config.numPixelsWide) *100)/100);
	    var y = (Math.round( (py / config.numPixelsHigh) *100)/100);

	    
	    return {
	    	x : x,
	    	y : y
	    };

	}

	var convertTouchesToPixels = function(touches) {

		var prevTouchedPixelsArray = touchedPixels;

		// clear out the touched pixels
		touchedPixels = [];

		// now find all the touched pixels
		for(var i=0; i<touches.length; i++) {
			var pixel = detectPixelTouched(i, touches[i]);

			var pixelIndex = allPixelsArray.findIndex(function(element, index) {
				return element.x == pixel.x && element.y == pixel.y;
			});

			touchedPixels.push(pixelIndex);

		}


		// show pixels as touched or not
		$('.pixel').css(config.noTouchStyle);
		for(var i=0;i<touchedPixels.length;i++) {
			$('#'+touchedPixels[i]).css(config.touchStyle);
		}

		// see if anything has changed (and pass to the onChange handler)
		var lengthsDiffer = (prevTouchedPixelsArray.length != touchedPixels.length); // this picks up the first touch scenerio
		var diff = _.difference(prevTouchedPixelsArray, touchedPixels);
		if (lengthsDiffer || diff.length>0) {

			config.onChange( convertIndexedPixelArrayToActualPixelsArray(touchedPixels), 
							 convertIndexedPixelArrayToActualPixelsArray(prevTouchedPixelsArray), 
							 diff );
		}


		// ALSO
		showTouches(touches);

	}

	var addPixelColours = function(pixel) {
		pixel.r = whyDoesDamianAlwaysInsistOnFloats(paintColor[0]);
		pixel.g = whyDoesDamianAlwaysInsistOnFloats(paintColor[1]);
		pixel.b = whyDoesDamianAlwaysInsistOnFloats(paintColor[2]);
		return pixel;
	}

	var convertIndexedPixelArrayToActualPixelsArray = function(indexArray) {
		var actualPixelArray = [];
		for (var i=0; i<indexArray.length; i++) {
			// actually, to make comparrison easier on the server - lets make them strings!
			var pix = allPixelsArray[indexArray[i]];
			pix = addPixelColours(pix);
			var pixelStr = JSON.stringify(pix);
			actualPixelArray.push(pixelStr);
		}
		return actualPixelArray;
	}


	var showTouch = function(finger, touchEvent) {
	
	    var tx = (touchEvent.pageX - 20)+"px";
	    var ty = (touchEvent.pageY - 20)+"px";

		// create if not exists
		if (typeof(visibleTouchCircles[finger])=='undefined') {
			var circle = $('<div class="touch"></div>');
			$(canvas).append(circle);
		} else {
			circle = visibleTouchCircles[finger];
		}

		circle
			.show()
			.css( { left: tx, top: ty });

		visibleTouchCircles.push(circle);
	}

	var showTouches = function(touchEvent) {

		$('.touch').hide();
		for (var i=0; i<touchEvent.length; i++) {
			showTouch(i, touchEvent[i]);
		}
	}




	var module = function() {};


	module.prototype.cacheDB = function() { return cacheDatabase; }

    // Application Constructor
    module.prototype.init = function(element, settings) {

    	if (typeof(settings)!='undefined') {
    		if (settings.numPixelsWide) config.numPixelsWide = settings.numPixelsWide;
    		if (settings.numPixelsHigh) config.numPixelsHigh = settings.numPixelsHigh;
    		if (settings.showTouches) config.showTouches = settings.showTouches;
    		if (settings.showPixels) config.showPixels = settings.showPixels;
    		if (settings.onChange) config.onChange = settings.onChange;
    	}


    	canvas = document.getElementById(element);

		canvas.addEventListener('touchend', function() {
		  event.preventDefault();
		  convertTouchesToPixels(event.touches);
		});
		canvas.addEventListener('touchmove', function(event) {
		  event.preventDefault();
		  convertTouchesToPixels(event.touches);
		});
		canvas.addEventListener('touchstart', function(event) {
		  event.preventDefault();
		  convertTouchesToPixels(event.touches);
		});

		// for browser testing !
		canvas.addEventListener('click', function(event) {
			event.preventDefault();
			var touchEvent = { pageX: event.clientX, pageY: event.clientY };
			var touches = [ touchEvent ];
			convertTouchesToPixels(touches);
		});

		config.canvas = canvas;

		createPixelDivs();

    };

    module.prototype.showMaster = function(array) {
		

		pixelIdArray = [];

		// now find all the touched pixels
		for(var i=0; i<array.length; i++) {
			var pixel = JSON.parse(array[i]); 

			var pixelIndex = allPixelsArray.findIndex(function(element, index) {
				return element.x == pixel.x && element.y == pixel.y;
			});

			pixelIdArray.push(pixelIndex);

		}


		$('.pixel').removeClass('masterTouch');

		for(var i=0;i<pixelIdArray.length;i++) {
			$('#'+pixelIdArray[i]).addClass('masterTouch');
		}

    }




	return module;

}) ();

var pixelTouchManager = new touchesDetector();



