
var save_instructions_module = (function (){

	var elem;


	// from : http://stackoverflow.com/questions/21741841/detecting-ios-android-operating-system
	var setSaveInstructions = function() {
	  var userAgent = navigator.userAgent || navigator.vendor || window.opera;

	  if( userAgent.match( /iPad/i ) || userAgent.match( /iPhone/i ) || userAgent.match( /iPod/i ) )
	  {
	  	/* The run-from-homescreen detection I'm doing is not working for iOS -- and this is not something to
	  	waste so much time on - so just don't do that for iOS *
	    document.getElementById(elem).innerHTML = "<h3>Save this to your phone home screen</h3>Tap the Share button on the browser's toolbar - that's the rectangle with an arrow pointing upward. <br/><br/>Tap the Add to Home Screen icon in the Share menu.";
	    */
	    document.getElementById(elem).innerHTML = "Touch the screen anywhere";
	  }
	  else if( userAgent.match( /Android/i ) )
	  {
		document.getElementById(elem).innerHTML = "<h3>Save this to your phone home screen</h3>Tap the menu button and tap Add to homescreen.";
	  }
	  else
	  {
	    document.getElementById(elem).innerHTML = "<h3>Save this to your home screen</h3>";
	  }
	}	

	var isRunningStandalone = function() {
	    return (window.matchMedia('(display-mode: standalone)').matches);
	}


	var module = function() {};

	module.prototype.init = function(element) {

		elem = element;

		setSaveInstructions();

		if (isRunningStandalone()) {
		    document.getElementById(elem).innerHTML = "Touch the screen anywhere";
		}		
	}

	return module;
	
}) ();

var saveToHomeScreenInstructionManager = new save_instructions_module();

