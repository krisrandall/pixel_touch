var paintColor = [255,255,255];


var dev_config = {
	socket_server : 'http://localhost:11001',
	pixel_touch_config : { numPixelsWide : 15,
						   numPixelsHigh : 20, 
						   onChange : publishChange }
}
var live_config = {
	socket_server : 'http://66.228.50.213:11001',
	pixel_touch_config : { numPixelsWide : 15,
						   numPixelsHigh : 20, 
						   onChange : publishChange }
}

var CONFIG = dev_config;



var socket = io.connect( CONFIG.socket_server );

console.log('Connected with config : ', CONFIG);

function publishChange(newPixelArray, oldPixelArray) {
	socket.emit('pixelArrayChange', { 'new' : newPixelArray, 'old' : oldPixelArray }); 
}


$(document).ready(function() {

	if ($('#save_instructions').length) {
		saveToHomeScreenInstructionManager.init('save_instructions');
	}

	pixelTouchManager.init('full_touch_body', CONFIG.pixel_touch_config );

});

$("#colorPicker").on("click", ".paletteOption", function () {
	var $this = $(this),
		color = $this.css("background-color");

	color = color.replace("rgb(", "").replace(")", "").replace(" ", "").split(',');
	paintColor = [parseInt(color[0]), parseInt(color[1]), parseInt(color[2])];
	$(".paletteOption").removeClass("selected");
	$this.addClass("selected");
});