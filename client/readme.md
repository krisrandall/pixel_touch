# The Pixel Touch Client

This is a web app which lets the user touch the screen and sends that multi touch info back to the pixel touch server via sockets.

**Simply open `index.html` in the browser to run the app.**

There is also a master view, `masterindex.html` which shows the consolidated pixel view -- this is intended as a debugging tool.

### Configuration

All config is at the top of `js/app.js`


#### Server Config

The `socket_server` must be set to where the server code is running


It is currently set to 'http://66.228.50.213:11001' for live


#### Pixel Touch Config

The pixel touch manager gets passed a the canvas element and a config object on init, the config options are :  

* numPixelsWide (these define the grid size / pixel size)
* numPixelsHigh (these define the grid size / pixel size)
* onChange (function to run when the pixel grid state changes)
* touchStyle (css to apply to touched pixels)
* noTouchStyle (css to apply to untouched pixels)

eg.
 
    pixelTouchManager.init(element, 
			{ numPixelsWide : 15,
			  numPixelsHigh : 20, 
			  onChange : publishChange } );
							  
							  
### Incomplete

* UI needs imporvement generally, but especially around prompting the user to save the page to their home screen
* .. I won't add more ... we will try and get the other things done tonight yet ..



